<!DOCTYPE html>
<html>
<head>
    <?php
    include_once "assets/partials/header.php";
    ?>
</head>

<body>
<div class="container">

    <div class="row">

        <div class="col-lg-6 col-md-6 col-sn-12 offset-lg-3 offset-md-3">
            <div class="content-center" id="1">
                <div class="itemContainer customCard">

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                            <h3>
                              <?php
                              if($_SESSION['language']=='ITA')
                                  echo 'Cosa vuoi scoprire?';
                                  if($_SESSION['language']=='ENG')
                                  echo 'What do you want to discover?';
                              ?>
                            </h3>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 mt-4">

                            <?php
                            $sql_aree = "SELECT * FROM aree";
                            $result = mysqli_query($conn, $sql_aree);

                            while ($row = mysqli_fetch_assoc($result)) {
                                echo '<p>&#8226; ';
                                echo '<a href="collezioni.php?id_area='.$row['id_area'].'">' . $row['nome_' . $_SESSION['language']] . '</a>';
                                echo '</p>';
                            }
                            ?>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

</div>

<?php
include_once "assets/partials/footer.html";
?>

</body>
</html><?php
