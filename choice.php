<!DOCTYPE html>
<html>
<head>
    <?php
    include_once "assets/partials/header.php";
    ?>
</head>

<body>
<div class="container">

    <div class="row">

        <div class="col-lg-8 col-md-8 col-sn-12 offset-lg-2 offset-md-2">
            <div class="content-center" id="choiceBox">
                <div class="customCard text-center">

                    <div class="row justify-content-center align-items-center">
                        <h3>Mostra dei saperi e delle Mirabilia Siciliane</h3>
                    </div>

                    <div class="mt-4">

                        <div class="row justify-content-center align-items-center">

                            <form method="POST" action="index.php">

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <h6>Lingua:</h6>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="language" id="ITA"
                                               value="ITA"
                                               checked>
                                        <label class="form-check-label" for="ITA">Italiano</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="language" id="ENG"
                                               value="ENG">
                                        <label class="form-check-label" for="ENG">English</label>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <h6 class="mt-4">Modalità:</h6>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="mode" id="adulti"
                                               value="adulti"
                                               checked>
                                        <label class="form-check-label" for="adulti">Adulto</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="mode" id="bambini"
                                               value="bambini">
                                        <label class="form-check-label" for="bambini">Bambino</label>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 mt-4">
                                    <button type="submit" class="btn-grad text-uppercase">Avanti</button>
                                </div>

                            </form>
                        </div>

                    </div>

                </div>
            </div>

        </div>

    </div>

</div>

<?php
include_once "assets/partials/footer.html";
?>

</body>
</html><?php
