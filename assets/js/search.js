$("#search").submit(function(event){
    // cancels the form submission
    event.preventDefault();
    submitForm();
});

function submitForm(){
    // Initiate Variables With Form Content
    let input = $("#input-search").val();

    $.ajax({
        type: "POST",
        url: "assets/partials/search-db.php",
        data: "input=" + input,
        success : function(msg){
            formSuccess(msg);
        }
    });
}
function formSuccess(msg){
    if(msg != "error\n"){
        $("#error").addClass("d-none");
        $( "#result-box" ).html(msg);
        $( "#result-box" ).show();
    }
    if(msg == "error\n"){
        $( "#result-box" ).hide();
        $("#error").removeClass("d-none");
    }
}