<?php
include_once "db_connection.php";

// controllo che la sessione sia attiva, altrimenti la attivo, per permettermi di utilizzare il multilingua
if(session_id() == '' || !isset($_SESSION)) {
    // session isn't started
    session_start();
}

$input = $_POST['input'];
$nome_multilang = "nome_".$_SESSION['language'];
$sql_oggetti = "SELECT * FROM oggetto WHERE $nome_multilang LIKE '%$input%'";
$result_oggetti = mysqli_query($conn, $sql_oggetti);
$num_row_oggetti = mysqli_num_rows($result_oggetti);

$sql_collezioni = "SELECT * FROM collezioni WHERE $nome_multilang LIKE '%$input%'";
$result_collezioni = mysqli_query($conn, $sql_collezioni);
$num_row_collezioni = mysqli_num_rows($result_collezioni);


if ($num_row_oggetti == 0 && $num_row_collezioni == 0) {
    echo "error";
}
else

?>

<?php
if ($num_row_oggetti != 0 || $num_row_collezioni != 0) :

?>

<div class="col-lg-8 col-md-8 col-sm-12 mb-5">
    <div class="customCard text-center">
        <div class="justify-content-center">

            <?php
            if($num_row_oggetti > 0):
                ?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>
                          <?php
                          if($_SESSION['language']=='ITA')
                              echo 'Oggetti';
                              if($_SESSION['language']=='ENG')
                              echo 'Objects';
                          ?>
                        </h3>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-lg-12 col-md-10 col-sm-12 text-left">
                        <?php
                        while($row = mysqli_fetch_assoc($result_oggetti)){
                            echo "<p class='mt-2'>- ";
                            echo '<a href="oggetto.php?id_oggetto='.$row['id_oggetto'].'">' . $row['nome_'.$_SESSION['language']] . '</a>';
                            echo "</p>";
                        }
                        ?>
                    </div>
                </div>

            <?php
            endif;
            ?>

            <?php
            if($num_row_collezioni > 0):
                ?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>
                          <?php
                          if($_SESSION['language']=='ITA')
                              echo 'Collezioni';
                              if($_SESSION['language']=='ENG')
                              echo 'Collections';
                          ?>
                        </h3>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-lg-12 col-md-10 col-sm-12 text-left">
                        <?php
                        while($row = mysqli_fetch_assoc($result_collezioni)){
                            echo "<p class='mt-2'>- ";
                            echo '<a href="collezione.php?id_collezione='.$row['id_collezione'].'">' . $row['nome_'.$_SESSION['language']] . '</a>';
                            echo "</p>";
                        }
                        ?>
                    </div>
                </div>

            <?php
            endif;
            ?>
        </div>
    </div>
</div>

<?php
endif;
?>
