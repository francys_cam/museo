<?php
$sql_oggetti_collezione = "SELECT * FROM oggetto WHERE id_collezione=$id_collezione";
$result_oggetti_collezione = mysqli_query($conn, $sql_oggetti_collezione);
$num_row = mysqli_affected_rows($conn);
if ($num_row > 0):

    ?>
    <div class="col-lg-8 col-md-8 col-sn-12 offset-lg-2 offset-md-2 mb-md-3 mb-5 itemsContainer">
        <div class="itemContainer customCard">
            <div class="row justify-content-center align-items-center blog">

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h3 class="text-center mb-md-5 mb-sm-3">
                      <?php
                      if($_SESSION['language']=='ITA')
                          echo 'Scopri gli oggetti della collezione';
                      if($_SESSION['language']=='ENG')
                      echo 'Discover the objects in the collection';
                      ?>
                      </h3>
                    <div id="desktopItemCarousel" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            <?php
                            $cont = 0;
                            while ($cont < $num_row / 4) {
                                echo '<li data-target="#desktopItemCarousel" data-slide-to="' . $cont . '"' . (($cont == 0) ? ' class="active"' : "") . '></li>';
                                $cont++;
                            }

                            ?>

                        </ol>

                        <div class="carousel-inner">
                            <?php
                            $cont = 0;
                            while ($row_oggetti_collezione = mysqli_fetch_assoc($result_oggetti_collezione)) {
                                $cont++;
                                if ($cont == 1) {
                                    echo '<div class="carousel-item active">';
                                    echo '<div class="row justify-content-center">';
                                } else {
                                    if (($cont - 1) % 3 == 0) {
                                        echo '<div class="carousel-item">';
                                        echo '<div class="row justify-content-center">';
                                    }

                                }
                                $id_oggetto = $row_oggetti_collezione['id_oggetto'];
                                $sql_img = "SELECT * FROM immagini WHERE id_oggetto=$id_oggetto";
                                $result_img = mysqli_query($conn, $sql_img);
                                $row_img = mysqli_fetch_assoc($result_img);
                                ?>
                                <div class="col-md-3 carouselItem">
                                    <a href="oggetto.php?id_oggetto=<?php echo $id_oggetto; ?>">
                                        <?php
                                        $img_path = $row_img['path'];
                                        if ($img_path == null)
                                            $img_path = "./assets/img/image_not_found.png";
                                        ?>

                                        <img src="<?php echo $img_path; ?>" alt="Image" style="max-width:100%;"
                                             class="img-items">
                                        <p class="text-center title mt-2"><?php echo $row_oggetti_collezione['nome_' . $_SESSION['language']] ?></p>
                                    </a>
                                </div>
                                <?php

                                if ($cont % 3 == 0 || $cont == $num_row) {
                                    echo '</div>';
                                    echo '</div>';
                                }
                            }

                            ?>

                        </div>

                    </div>

                    <div id="mobileItemCarousel" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            <?php
                            $sql_oggetti_collezione = "SELECT * FROM oggetto WHERE id_collezione=$id_collezione";
                            $result_oggetti_collezione = mysqli_query($conn, $sql_oggetti_collezione);
                            $cont = 0;
                            while ($cont < $num_row) {
                                echo '<li data-target="#mobileItemCarousel" data-slide-to="' . $cont . '"' . (($cont == 0) ? ' class="active"' : "") . '></li>';
                                $cont++;
                            }

                            ?>

                        </ol>

                        <div class="carousel-inner">
                            <?php
                            $cont = 0;
                            while ($row_oggetti_collezione = mysqli_fetch_assoc($result_oggetti_collezione)) {
                            $cont++;
                            if ($cont == 1) {
                                echo '<div class="carousel-item active">';
                                echo '<div class="row justify-content-center">';
                            } else {
                                echo '<div class="carousel-item">';
                                echo '<div class="row justify-content-center">';
                            }
                            $id_oggetto = $row_oggetti_collezione['id_oggetto'];
                            $sql_img = "SELECT * FROM immagini WHERE id_oggetto=$id_oggetto";
                            $result_img = mysqli_query($conn, $sql_img);
                            $row_img = mysqli_fetch_assoc($result_img);
                            ?>
                            <div class="col-10 carouselItem">
                                <a href="oggetto.php?id_oggetto=<?php echo $id_oggetto; ?>">
                                    <?php
                                    $img_path = $row_img['path'];
                                    if ($img_path == null)
                                        $img_path = "./assets/img/image_not_found.png";
                                    ?>

                                    <img src="<?php echo $img_path; ?>" alt="Image" style="max-width:100%;"
                                         class="img-items">
                                    <p class="text-center title mt-2"><?php echo $row_oggetti_collezione['nome_' . $_SESSION['language']] ?></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                    ?>

                </div>

            </div>

        </div>

    </div>

<?php
endif;
?>
