<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="utf-8">
<title>Mostra dei saperi e delle Mirabilia Siciliane</title>

<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;700&display=swap" rel="stylesheet">
<?php
include_once "assets/partials/navbar.html";
include_once "assets/partials/db_connection.php";

$curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);

if(session_id() == '' || !isset($_SESSION)) {
    // session isn't started
    session_start();
}

if(!isset($_SESSION['language']) && !isset($_SESSION['mode']) && strcmp($curPageName, "choice.php") !== 0){ // questo controla se la variabile di sessione esiste ed è settata, se è una nuova sessione non fa accedere a nessuna pagina e reindirizza alla pagina di scelta delle impostazioni
    header("Location: http://localhost:8888/museo/choice.php");
//    exit();
}
?>
