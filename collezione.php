<!DOCTYPE html>
<html>
<head>
    <?php
    include_once "assets/partials/header.php";
    ?>
</head>

<body>
<div class="container custom-container">

    <div class="row">

        <div class="col-lg-8 col-md-8 col-sn-12 offset-lg-2 offset-md-2">
            <div class="customCard">
                <div class="row">
                    <?php
                    $id_collezione = $_GET['id_collezione'];

                    $sql_collezione = "SELECT * FROM collezioni WHERE id_collezione=$id_collezione";
                    $result_collezione = mysqli_query($conn, $sql_collezione);
                    $row_collezione = mysqli_fetch_assoc($result_collezione);

                    ?>
                    <div class="col-lg-10 col-md-12 col-sm-12 offset-lg-1">
                        <h3 class="title text-center">
                            <?php
                            echo $row_collezione['nome_' . $_SESSION['language']];
                            ?>
                        </h3>

                        <p class="description">
                            <?php
                            echo $row_collezione['descrizione_' . $_SESSION['language'] . '_adulti'];
                            ?>
                        </p>
                    </div>

                    <?php
                    $id_affidatario = $row_collezione['id_affidatario'];
                    $sql_affidatario = "SELECT * FROM affidatari WHERE id_affidatario = '$id_affidatario'";
                    $result_affidatario = mysqli_query($conn, $sql_affidatario);
                    $row_affidatario = mysqli_fetch_assoc($result_affidatario);
                    ?>
                    <div class="col-lg-10 col-md-12 col-sm-12 offset-lg-1 mt-5">
                        <?php
                        if ($row_affidatario) {
                            ?>

                            <a href="<?php echo $row_affidatario['link'] ?>">
                                <h5><?php echo $row_affidatario['nome_' . $_SESSION['language']]; ?></h5>
                            </a>
                            <?php
                        }
                        ?>

                    </div>

                </div>

            </div>

        </div>

        <?php
        include_once "assets/partials/display_item.php";
        ?>

    </div>

</div>

</div>

<?php
include_once "assets/partials/footer.html";
?>

</body>
</html>
