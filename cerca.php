<!DOCTYPE html>
<html>
<head>
    <?php
    include_once "assets/partials/header.php";
    ?>
</head>

<body>
<div class="container custom-container">

    <div class="row justify-content-center align-items-center">

        <div class="col-lg-8 col-md-8 col-sm-12">
            <div class="customCard text-center">
                <div class="justify-content-center">

                    <form id="search" method="POST">

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <h3>
                                  <?php
                                  if($_SESSION['language']=='ITA')
                                      echo 'Cerca oggetti o collezioni';
                                      if($_SESSION['language']=='ENG')
                                      echo 'Search objects or collections';
                                  ?>
                                </h3>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-lg-12 col-md-10 col-sm-12">
                                <div class="form-group row justify-content-center">
                                    <div class="col-lg-9 col-md-9 co-sm-8 col-12 d-flex align-items-center">
                                        <input id="input-search" name="input-search" placeholder="Inserisci testo per la ricerca" type="text" class="form-control" required="required">
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-12 mt-sm-0 mt-3">
                                        <button type="submit" class="btn-grad" id="search-submit">
                                          <?php
                                          if($_SESSION['language']=='ITA')
                                              echo 'Cerca';
                                              if($_SESSION['language']=='ENG')
                                              echo 'Search';
                                          ?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 d-none" id="error">
                                <p>  <?php
                                  if($_SESSION['language']=='ITA')
                                      echo 'Nessun risultato corrispondente';
                                      if($_SESSION['language']=='ENG')
                                      echo 'No results';
                                  ?>
                                </p>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>

    </div>

    <div class="row justify-content-center align-items-center resultBox" id="result-box">

    </div>

</div>

</div>

<?php
include_once "assets/partials/footer.html";
?>

</body>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/search.js"></script>
</html>
