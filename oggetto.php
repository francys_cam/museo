<!DOCTYPE html>
<html>
<head>
    <?php
    include_once "assets/partials/header.php";
    ?>
</head>

<body>
<div class="container custom-container">

    <div class="row">

        <div class="col-lg-8 col-md-8 col-sn-12 offset-lg-2 offset-md-2">
            <div class="customCard text-center">
                <div class="row justify-content-center align-items-center">
                    <?php
                    $id_oggetto = $_GET['id_oggetto'];

                    $sql_item = "SELECT * FROM oggetto WHERE id_oggetto=$id_oggetto";
                    $result_item = mysqli_query($conn, $sql_item);
                    $row_item = mysqli_fetch_assoc($result_item);
                    $id_collezione = $row_item['id_collezione'];

                    $sql_img = "SELECT path FROM immagini WHERE id_oggetto=$id_oggetto";
                    $result_img = mysqli_query($conn, $sql_img);
                    $row_img = mysqli_fetch_assoc($result_img);

                    ?>
                    <div class="col-lg-11 col-md-12 col-sm-12">
                        <h3 class="title">
                            <?php
                            echo $row_item['nome_' . $_SESSION['language']];
                            ?>
                        </h3>

                        <p class="description mt-4">
                            <?php
                            echo $row_item['descrizione_' . $_SESSION['language'] . '_adulti'];
                            ?>
                        </p>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 mt-md-4 mt-3 mb-3">
                        <img src="<?php echo $row_img['path'] ?>" class="img-item img-fluid">
                    </div>

                </div>

            </div>

        </div>

        <?php

        include_once "assets/partials/display_item.php";

        ?>

    </div>

</div>

<?php
include_once "assets/partials/footer.html";
?>

</body>
</html>