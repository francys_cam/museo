<!DOCTYPE html>
<html>
<head>
    <?php
    include_once "assets/partials/header.php";
    ?>
</head>

<body>
<div class="container">

    <div class="row">

        <div class="col-lg-6 col-md-6 col-sn-12 offset-lg-3 offset-md-3">
            <div class="content-center" id="1">
                <div class="itemContainer customCard">

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                            <?php
                            $id_area = $_GET['id_area'];
                            $sql_area = "SELECT * FROM aree WHERE id_area = $id_area";
                            $result_area = mysqli_query($conn, $sql_area);
                            $row_area = mysqli_fetch_assoc($result_area);
                            ?>
                            <h3>
                                <?php echo $row_area['nome_' . $_SESSION['language']] ?>
                            </h3>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 mt-4">
                          <!--  <p>
                              <?php
                              if($_SESSION['language']=='ITA')
                                  echo 'Collezioni';
                                 if($_SESSION['language']=='ENG')
                                echo 'Collections';
                              ?>
                            </p>
-->
                            <?php
                            $sql_collezioni = "SELECT * FROM collezioni WHERE id_area = $id_area";
                            $result_sollezioni = mysqli_query($conn, $sql_collezioni);

                            while ($row = mysqli_fetch_assoc($result_sollezioni)) {
                                echo '<p>&#8226; ';
                                echo '<a href="collezione.php?id_collezione='.$row['id_collezione'].'">' . $row['nome_' . $_SESSION['language']] . '</a>';
                                echo '</p>';
                            }
                            ?>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

</div>

<?php
include_once "assets/partials/footer.html";
?>

</body>
</html><?php
