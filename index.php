<!DOCTYPE html>
<html>
<head>
    <?php

    if(session_id() == '' || !isset($_SESSION)) {
        // session isn't started
        session_start();
    }

    if (!empty($_POST["language"]) && !empty($_POST["mode"])) {
        $_SESSION["language"] = $_POST['language'];
        $_SESSION["mode"] = $_POST['mode'];
    }

    include_once "assets/partials/header.php";

    ?>
</head>

<body>
<div class="container">

    <div class="row">

        <div class="col-lg-6 col-md-6 col-sn-12 offset-lg-3 offset-md-3">

            <div class="content-center" id="homeBox">

                <div class="customCard text-center">

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <h3>
                                <?php
                                // impostazione lingua
                                if ($_SESSION['language'] == 'ITA')
                                    echo 'Benvenuto a Mirabilia!';
                                if ($_SESSION['language'] == 'ENG')
                                    echo 'Welcome in Mirabilia!'
                                ?>

                            </h3>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 mt-3 align-items-center justify-content-center">
                            <a type="button" class="btn-grad text-uppercase"
                               href="percorsi_didattici.php">
                                <?php
                                if ($_SESSION['language'] == 'ITA')
                                    echo 'Le collezioni';
                                if ($_SESSION['language'] == 'ENG')
                                    echo 'Collections';
                                ?>
                            </a>

                            <a type="button" class="btn-grad text-uppercase"
                               href="storia_museo.php">
                                <?php
                                if ($_SESSION['language'] == 'ITA')
                                    echo 'Storia del museo';
                                if ($_SESSION['language'] == 'ENG')
                                    echo 'Museum\'s History';
                                ?>
                            </a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<?php
include_once "assets/partials/footer.html";
?>

</body>
</html><?php
