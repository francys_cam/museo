<!DOCTYPE html>
<html>
<head>
    <?php
    include_once "assets/partials/header.php";
    ?>
</head>

<body>
<div class="container content-center">

    <div class="row">

        <div class="col-lg-8 col-md-8 col-sn-12 offset-lg-2 offset-md-2">
            <div class="customCard text-center">
                <div class="row justify-content-center align-items-center">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <h3>
                                <?php
                                // impostazione lingua
                                if ($_SESSION['language'] == 'ITA')
                                    echo 'Storia del Museo';
                                if ($_SESSION['language'] == 'ENG')
                                    echo 'History of Museum'
                                ?>
                            </h3>
                            <div class="row">
                                <div class="col-lg-10 col-md-10 col-sm-12 offset-lg-1 offset-md-1">
                                    <p class="mt-3">
                                        <?php
                                        // impostazione lingua
                                        if ($_SESSION['language'] == 'ITA')
                                            echo 'La Mostra dei Saperi delle Mirabilie Siciliane situata presso Il Palazzo Centrale, nasce grazie al contributo del Sistema Museale di Ateneo (SiMuA) e dell Assessorato ai Beni culturali e all Identità Siciliana della Regione Siciliana.
Varcando le varie stanze si troveranno un susseguirsi di collezioni scientifiche, archeologiche, botaniche, minerali e naturalistiche, che vogliono offrire al pubblico una prima, ma concreta idea di quel cospicuo patrimonio dei saperi e dei beni che sono il frutto dello svolgersi della secolare attività di ricerca, di didattica e di divulgazione dell Ateneo catanese, il più antico della Sicilia.
All’interno della mostra, troviamo inoltre diversi percorsi didattici-educativi che offrono al visitatore un assaggio di tutto ciò che è esposto nelle varie strutture museali universitarie già esistenti.
';
                                        if ($_SESSION['language'] == 'ENG')
                                            echo 'Mostra dei Saperi e delle Mirabilie Siciliane located at the Central Palace was born thanks to the contribution of the University Museum System (SiMuA) and the Department of Cultural Heritage and Sicilian Identity of the Sicilian Region.
Crossing the various rooms you will find a succession of scientific, archaeological, botanical, mineral and naturalistic collections, which want to offer the public a first, but concrete idea of ​​that conspicuous heritage of knowledge and goods that are the result of the development of the centuries-old activity of research, teaching and dissemination of the Catania University, the oldest in Sicily.
Within the exhibition, we also find several didactic-educational itineraries that offer the visitor a taste of everything that is exhibited in the various already existing university museum structures.
';
                                        ?>

                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <?php
    include_once "assets/partials/footer.html";
    ?>

</body>
</html>
